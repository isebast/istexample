package com.istexample.bean;

import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("ResponseError")
public class ResponseError {
	
	private String status;
	private int code;
	private String  message;
	
	
	public ResponseError() {
		super();
	}

	public ResponseError(String status, int code, String message) {
		super();
		this.status = status;
		this.code = code;
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}
