package com.istexample.app;

public class DataSource {

	private String message;
	private String url;
	public DataSource() {
		super();
	}
	public DataSource(String message, String url) {
		super();
		this.message = message;
		this.url = url;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	
}
