package com.istexample.app;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfig {
	
	@Value("${dataSource.name}")
	private String message;
	
	@Value("${dataSource.url}")
	private String url;
	
	
	
	
	@Bean
	DataSource dataSources() {		
		return new DataSource(message, url);
	}

}
