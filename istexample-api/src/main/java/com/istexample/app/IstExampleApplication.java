package com.istexample.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages= {"com.istexample.app", "com.istexample.bean", "com.istexample.controller", "com.istexample.service"})
public class IstExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(IstExampleApplication.class, args);
	}
}