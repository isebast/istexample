package com.istexample.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.istexample.bean.ResponseError;
import com.istexample.service.IstServiceMessage;

@RestController
public class GetMessageController {

	private static final Logger LOGGER = LoggerFactory.getLogger(GetMessageController.class);
	
	@Autowired
	IstServiceMessage istServiceMessage;
	
	
	@Value("${app.title}")
	private String nameAplication;
	
	
	@GetMapping("/")
	public String getMessage() {
		
		return istServiceMessage.getMessage();
	}	
	
	//este ya es un ejemplo aparte al primer metodo recordar el metodo de arriba   muestra como podemos inyectar un servicio 
	//es decir un proyecto dentro de otro para usarlo  es decir ver en el pom   como lo importamos luego lo inyectamos y 
	//tambien hay que scanearlo en la clase main del boot
	//@SpringBootApplication(scanBasePackages= {"com.istexample.app", "com.istexample.controller", "com.istexample.service"})
	
	
	//a continuacion vamos a probar metodos de respuesta    para el proyecto  respuesta de error  y respuesta de la clase	
	//voy a definir un packete con las clase que seran devueltas el paquete sera bean
	
	@GetMapping("/prueba")
	public ResponseError getError() {
		
		LOGGER.debug("init getError");
		LOGGER.debug("prueba de la nueva rama");
		String message = nameAplication;
		
		ResponseError re = new ResponseError();		
		re.setCode(80);
		re.setMessage(message);
		re.setStatus("ko");
		LOGGER.debug("resultado correcto");	
		return re;	
	}	
}
